package kz.aura.crm.model;

public class SelectItem {
	private Long id;
	private String value;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public SelectItem(Long id, String value) {
		super();
		this.id = id;
		this.value = value;
	}

}
