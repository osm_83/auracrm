package kz.aura.crm.model;

import java.util.List;
import java.util.Objects;

public class Grid<T> {
	private final static int PER_PAGE_BTN_COUNT = 5;
	public final static int PER_PAGE_RECORD_COUNT = 50;
	private int totalPages;
	private int currentPage;
	private long totalRecords;
	private List<T> data;
	private String sortBy;
	private String sortDir;

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public long getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(long totalRecords) {
		this.totalRecords = totalRecords;
	}

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

	public String getSortDir() {
		return sortDir;
	}

	public void setSortDir(String sortDir) {
		this.sortDir = sortDir;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public boolean enablePrevBtn() {
		return currentPage > 1;
	}

	public boolean enableNextBtn() {
		return getCurrentPage() < getTotalPages();
	}

	public String getPrevPageUrl() {
		return "/city?p=" + (currentPage - 1) + "&sd=" + Objects.toString(getSortDir(),"") + "&sf="
				+ Objects.toString(getSortBy(),"");
	}

	public String getNextPageUrl() {
		return "/city?p=" + (currentPage + 1) + "&sd=" + Objects.toString(getSortDir(),"") + "&sf="
				+ Objects.toString(getSortBy(),"");
	}

	public int getCounterFrom() {
		if (getCurrentPage() <= PER_PAGE_BTN_COUNT) {
			return 1;
		} else {
			int temp = getTotalPages() < (getCurrentPage() + 4) ? getTotalPages() : getCurrentPage() + 4;
			return temp - 9 > 0 ? temp - 9 : 1;
		}
	}

	public int getCounterTo() {
		if (getCurrentPage() <= PER_PAGE_BTN_COUNT) {
			return getTotalPages() > 10 ? 10 : getTotalPages();
		} else {
			return getTotalPages() < (getCurrentPage() + 4) ? getTotalPages() : getCurrentPage() + 4;
		}
	}
}