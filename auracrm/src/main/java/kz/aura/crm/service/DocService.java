package kz.aura.crm.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import kz.aura.crm.entity.DocDemo;
import kz.aura.crm.entity.DocRec;
import kz.aura.crm.entity.iface.IDoc;

public interface DocService {
	
	DocDemo save(DocDemo entity);
	
	IDoc findOne(Long id);
	
	Page<DocDemo> findAll(Pageable pageable);
	
	List<DocRec> findAllRec();
}
