package kz.aura.crm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

import kz.aura.crm.entity.DocDemo;
import kz.aura.crm.entity.DocRec;
import kz.aura.crm.entity.iface.IDoc;
import kz.aura.crm.repository.DemoRepository;
import kz.aura.crm.repository.RecommendationRepository;
import kz.aura.crm.service.DocService;

@Service("docService")
@Repository
@Transactional
public class DocServiceImpl implements DocService {

	@Autowired
	DemoRepository demoRepository;

	@Autowired
	RecommendationRepository recommendationRepository;

	@Override
	public DocDemo save(DocDemo entity) {
		return demoRepository.save(entity);
	}

	@Override
	public IDoc findOne(Long id) {
		return null;
	}

	@Override
	public Page<DocDemo> findAll(Pageable pageable) {
		return demoRepository.findAll(pageable);
	}

	@Override
	public List<DocRec> findAllRec() {
		return Lists.newArrayList(recommendationRepository.findAll());
	}

}
