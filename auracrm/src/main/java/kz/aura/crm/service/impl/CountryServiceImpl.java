package kz.aura.crm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

import kz.aura.crm.entity.RefCountry;
import kz.aura.crm.repository.CountryRepository;
import kz.aura.crm.service.CountryService;
import kz.aura.crm.service.ReferenceService;

@Service("countryService")
@Repository
@Transactional
public class CountryServiceImpl implements CountryService {

	@Autowired
	private CountryRepository countryRepository;

	@Transactional(readOnly = true)
	@Override
	public List<RefCountry> findAll() {
		return Lists.newArrayList(countryRepository.findAll());
	}

	@Override
	public RefCountry save(RefCountry country) {
		return countryRepository.save(country);
	}

	@Transactional(readOnly = true)
	@Override
	public RefCountry findOne(Long id) {
		return countryRepository.findOne(id);
	}

	@Override
	public List<RefCountry> findByCountryId(Long countryId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<RefCountry> findAll(Pageable pageable) {
		return countryRepository.findAll(pageable);
	}

}
