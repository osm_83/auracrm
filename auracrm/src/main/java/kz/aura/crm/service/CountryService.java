package kz.aura.crm.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import kz.aura.crm.entity.RefCountry;

public interface CountryService {
	List<RefCountry> findAll();
	
	List<RefCountry> findByCountryId(Long countryId);
	
	Page<RefCountry> findAll(Pageable pageable);

	RefCountry save(RefCountry entity);

	RefCountry findOne(Long id);

}
