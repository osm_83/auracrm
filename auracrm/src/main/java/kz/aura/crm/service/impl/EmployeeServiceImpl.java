package kz.aura.crm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

import kz.aura.crm.entity.Employee;
import kz.aura.crm.repository.EmployeeRepository;
import kz.aura.crm.service.EmployeeService;

@Service("employeeService")
@Repository
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeRepository employeeRepository;

	@Override
	public List<Employee> findAll() {
		return Lists.newArrayList(employeeRepository.findAll());
	}

	@Override
	public List<Employee> findByBranchId(Long branchId) {
		//return Lists.newArrayList(employeeRepository.findAll());
		return null;
	}

	@Override
	public Employee findOne(Long id) {
		return employeeRepository.findOne(id);
	}

}
