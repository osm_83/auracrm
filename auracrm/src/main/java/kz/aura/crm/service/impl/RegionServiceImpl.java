package kz.aura.crm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

import kz.aura.crm.entity.RefRegion;
import kz.aura.crm.repository.RegionRepository;
import kz.aura.crm.service.RegionService;

@Service("regionService")
@Repository
@Transactional
public class RegionServiceImpl implements RegionService {

	@Autowired
	private RegionRepository regionRepository;
	
	@Override
	public List<RefRegion> findAll() {
		return Lists.newArrayList(regionRepository.findAll());
	}

	@Override
	public RefRegion save(RefRegion entity) {
		return regionRepository.save(entity);
	}

	@Override
	public RefRegion findOne(Long id) {
		return regionRepository.findOne(id);
	}

	@Override
	public List<RefRegion> findByCountryId(Long countryId) {
		return Lists.newArrayList(regionRepository.findByCountryId(countryId));
	}

}
