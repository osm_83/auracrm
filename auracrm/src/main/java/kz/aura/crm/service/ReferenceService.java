package kz.aura.crm.service;

import java.util.List;

public interface ReferenceService<T> {

	List<T> findAll();
	
	T save(T entity);
	
	T findOne(Long id);
}
