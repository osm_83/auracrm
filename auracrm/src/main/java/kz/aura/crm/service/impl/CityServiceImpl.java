package kz.aura.crm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

import kz.aura.crm.entity.RefCity;
import kz.aura.crm.repository.CityRepository;
import kz.aura.crm.service.CityService;
import kz.aura.crm.service.ReferenceService;

@Service("cityService")
@Repository
@Transactional
public class CityServiceImpl implements CityService {

	@Autowired
	private CityRepository cityRepository;
	
	@Override
	public List<RefCity> findAll() {
		return Lists.newArrayList(cityRepository.findAll());
	}

	@Override
	public RefCity save(RefCity entity) {
		return cityRepository.save(entity);
	}

	@Override
	public RefCity findOne(Long id) {
		return cityRepository.findOne(id);
	}

	@Override
	public List<RefCity> findByCountryId(Long countryId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<RefCity> findAll(Pageable pageable) {
		return cityRepository.findAll(pageable);
	}

}
