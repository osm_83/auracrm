package kz.aura.crm.service;

import java.util.List;

import kz.aura.crm.entity.Employee;

public interface EmployeeService {
	List<Employee> findAll();
	
	List<Employee> findByBranchId(Long branchId);

	Employee findOne(Long id);

}
