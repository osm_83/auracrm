package kz.aura.crm.service;

import java.util.List;

import kz.aura.crm.entity.RefRegion;

public interface RegionService {
	List<RefRegion> findAll();
	
	List<RefRegion> findByCountryId(Long countryId);

	RefRegion save(RefRegion entity);

	RefRegion findOne(Long id);

}
