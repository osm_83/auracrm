package kz.aura.crm.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import kz.aura.crm.entity.RefCity;

public interface CityService {
	List<RefCity> findAll();
	
	List<RefCity> findByCountryId(Long countryId);
	
	Page<RefCity> findAll(Pageable pageable);

	RefCity save(RefCity entity);

	RefCity findOne(Long id);

}
