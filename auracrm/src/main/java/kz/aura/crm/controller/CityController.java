package kz.aura.crm.controller;

import java.util.ArrayList;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.common.collect.Lists;

import kz.aura.crm.entity.RefCity;
import kz.aura.crm.entity.RefCountry;
import kz.aura.crm.model.Grid;
import kz.aura.crm.service.CityService;
import kz.aura.crm.service.CountryService;
import kz.aura.crm.service.ReferenceService;
import kz.aura.crm.service.RegionService;

@Controller
@RequestMapping("/city")
public class CityController {

	@PostConstruct
	public void init() {

	}

	@Autowired
	CityService cityService;

	@Autowired
	CountryService countryService;

	@Autowired
	RegionService regionService;

	@RequestMapping(method = RequestMethod.GET)
	public String index(Model model, @RequestParam(value = "p", required = false) Integer p,
			@RequestParam(value = "sf", required = false) String sortBy,
			@RequestParam(value = "sd", required = false) String dir) {

		Integer page = p == null ? 1 : p;
		Sort sort = null;
		if (sortBy != null && sortBy.length() > 0 && dir != null && dir.length() > 0) {
			sort = new Sort("asc".equals(dir) ? Sort.Direction.ASC : Sort.Direction.DESC, sortBy);
		}

		PageRequest pageRequest = null;
		if (sort == null) {
			pageRequest = new PageRequest(page - 1, 5);
		} else {
			pageRequest = new PageRequest(page - 1, 5, sort);
		}

		Page<RefCity> cityPage = cityService.findAll(pageRequest);
		Grid<RefCity> grid = new Grid<RefCity>();
		grid.setCurrentPage(cityPage.getNumber() + 1);
		grid.setTotalPages(cityPage.getTotalPages());
		grid.setTotalRecords(cityPage.getTotalElements());
		grid.setData(Lists.newArrayList(cityPage.iterator()));
		grid.setSortBy(sortBy);
		grid.setSortDir(dir);

		model.addAttribute("grid", grid);
		// model.addAttribute("items", cityService.findAll(pageRequest));
		model.addAttribute("pageTitle", "Список городов");
		return "city/grid";
	}

	@RequestMapping(path = "/add", method = RequestMethod.GET)
	public String add(Model model) {
		model.addAttribute("item", new RefCity());
		model.addAttribute("pageTitle", "Добавление города");
		model.addAttribute("regions", new ArrayList<>());
		model.addAttribute("countries", countryService.findAll());
		return "city/form";
	}

	@RequestMapping(path = "/update/{id}", method = RequestMethod.GET)
	public String update(@PathVariable("id") Long id, Model model) {
		RefCity city = cityService.findOne(id);
		model.addAttribute("item", city);
		model.addAttribute("pageTitle", "Редактирование города " + city.getName());
		model.addAttribute("regions", regionService.findByCountryId(city.getCountryId()));
		model.addAttribute("countries", countryService.findAll());
		return "city/form";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(@ModelAttribute("item") RefCity city) {
		cityService.save(city);

		return "redirect:/city";
	}
}
