package kz.aura.crm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/company")
public class CompanyController {

	@RequestMapping(value="/structure",method=RequestMethod.GET)
	public String structure(){
		return "company/structure";
	}
	
	@RequestMapping(value="/employee",method=RequestMethod.GET)
	public String employee(){
		
		return "company/employee";
	}
}
