package kz.aura.crm.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import kz.aura.crm.entity.DocRec;
import kz.aura.crm.entity.Employee;
import kz.aura.crm.model.SelectItem;
import kz.aura.crm.service.DocService;
import kz.aura.crm.service.EmployeeService;

@Controller
@RequestMapping("/recommendation")
public class RecommendationController {

	@Autowired
	DocService docService;

	@Autowired
	EmployeeService employeeService;

	@RequestMapping(method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute("items", docService.findAllRec());
		return "recommendation/index";
	}

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String add(Model model) {
		List<SelectItem> dealers = new ArrayList<>();
		for (Employee emp : employeeService.findAll()) {
			SelectItem si = new SelectItem(emp.getId(), emp.getLastname() + " " + emp.getFirstname());
			dealers.add(si);
		}
		model.addAttribute("item", new DocRec());
		model.addAttribute("dealers", dealers);
		return "recommendation-form";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, String> save(@RequestBody DocRec docRec) {
		System.out.println(docRec);
		Map<String, String> out = new HashMap<String, String>();
		out.put("status", "1");
		return out;
	}
}
