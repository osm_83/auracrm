package kz.aura.crm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/customer")
public class CustomerController {

	@RequestMapping(method=RequestMethod.GET)
	public String index(){
		
		return "customer/index";
	}
	
	@RequestMapping(value="/birthday",method=RequestMethod.GET)
	public String birthday(){
		
		return "customer/birthday";
	}
	
	@RequestMapping(value="/mail",method=RequestMethod.GET)
	public String mail(){
		
		return "customer/mail";
	}
}
