package kz.aura.crm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;

import kz.aura.crm.entity.RefCountry;
import kz.aura.crm.model.Grid;
import kz.aura.crm.service.CountryService;

@Controller
@RequestMapping("/country")
public class CountryController {

	@Autowired
	CountryService countryService;

	@RequestMapping(method = RequestMethod.GET)
	public String index(Model model, @RequestParam(value = "p", required = false) Integer p,
			@RequestParam(value = "sf", required = false) String sortBy,
			@RequestParam(value = "sd", required = false) String dir) {

		Integer page = p == null ? 1 : p;
		Sort sort = null;
		if (sortBy != null && sortBy.length() > 0 && dir != null && dir.length() > 0) {
			sort = new Sort("asc".equals(dir) ? Sort.Direction.ASC : Sort.Direction.DESC, sortBy);
		}

		PageRequest pageRequest = null;
		if (sort == null) {
			pageRequest = new PageRequest(page - 1, Grid.PER_PAGE_RECORD_COUNT);
		} else {
			pageRequest = new PageRequest(page - 1, Grid.PER_PAGE_RECORD_COUNT, sort);
		}

		Page<RefCountry> cityPage = countryService.findAll(pageRequest);
		Grid<RefCountry> grid = new Grid<RefCountry>();
		grid.setCurrentPage(cityPage.getNumber() + 1);
		grid.setTotalPages(cityPage.getTotalPages());
		grid.setTotalRecords(cityPage.getTotalElements());
		grid.setData(Lists.newArrayList(cityPage.iterator()));
		grid.setSortBy(sortBy);
		grid.setSortDir(dir);

		model.addAttribute("grid", grid);
		model.addAttribute("pageTitle", "Список стран");

		return "country/grid";
	}
	
	@RequestMapping(value="/test-grid",method = RequestMethod.GET)
	public String testGrid(Model model, @RequestParam(value = "p", required = false) Integer p,
			@RequestParam(value = "sf", required = false) String sortBy,
			@RequestParam(value = "sd", required = false) String dir) {

		Integer page = p == null ? 1 : p;
		Sort sort = null;
		if (sortBy != null && sortBy.length() > 0 && dir != null && dir.length() > 0) {
			sort = new Sort("asc".equals(dir) ? Sort.Direction.ASC : Sort.Direction.DESC, sortBy);
		}

		PageRequest pageRequest = null;
		if (sort == null) {
			pageRequest = new PageRequest(page - 1, Grid.PER_PAGE_RECORD_COUNT);
		} else {
			pageRequest = new PageRequest(page - 1, Grid.PER_PAGE_RECORD_COUNT, sort);
		}

		Page<RefCountry> cityPage = countryService.findAll(pageRequest);
		Grid<RefCountry> grid = new Grid<RefCountry>();
		grid.setCurrentPage(cityPage.getNumber() + 1);
		grid.setTotalPages(cityPage.getTotalPages());
		grid.setTotalRecords(cityPage.getTotalElements());
		grid.setData(Lists.newArrayList(cityPage.iterator()));
		grid.setSortBy(sortBy);
		grid.setSortDir(dir);

		model.addAttribute("grid", grid);
		model.addAttribute("pageTitle", "Список стран");

		return "reference/country/gridBody";
	}
	
	@RequestMapping(value="/test",method = RequestMethod.GET)
	public String test(Model model){
		model.addAttribute("pageTitle","Test Grid Page");
		model.addAttribute("addUrl","/country/add");
		return "country/test";
	}
	
	@RequestMapping(value="/grid",method = RequestMethod.GET)
	@ResponseBody
	public Grid<RefCountry> grid(Model model, @RequestParam(value = "p", required = false) Integer p,
			@RequestParam(value = "sf", required = false) String sortBy,
			@RequestParam(value = "sd", required = false) String dir) {

		Integer page = p == null ? 1 : p;
		Sort sort = null;
		if (sortBy != null && sortBy.length() > 0 && dir != null && dir.length() > 0) {
			sort = new Sort("asc".equals(dir) ? Sort.Direction.ASC : Sort.Direction.DESC, sortBy);
		}

		PageRequest pageRequest = null;
		if (sort == null) {
			pageRequest = new PageRequest(page - 1, Grid.PER_PAGE_RECORD_COUNT);
		} else {
			pageRequest = new PageRequest(page - 1, Grid.PER_PAGE_RECORD_COUNT, sort);
		}

		Page<RefCountry> cityPage = countryService.findAll(pageRequest);
		Grid<RefCountry> grid = new Grid<RefCountry>();
		grid.setCurrentPage(cityPage.getNumber() + 1);
		grid.setTotalPages(cityPage.getTotalPages());
		grid.setTotalRecords(cityPage.getTotalElements());
		grid.setData(Lists.newArrayList(cityPage.iterator()));
		grid.setSortBy(sortBy);
		grid.setSortDir(dir);
		return grid;

//		model.addAttribute("grid", grid);
//		model.addAttribute("pageTitle", "Список стран");
//
//		return "country/grid";
	}

	@RequestMapping(path = "/add", method = RequestMethod.GET)
	public String add(Model model) {
		model.addAttribute("item", new RefCountry());
		return "country/form";
	}

	@RequestMapping(path = "/update/{id}", method = RequestMethod.GET)
	public String update(@PathVariable("id") Long id, Model model) {
		RefCountry country = countryService.findOne(id);
		model.addAttribute("item", country);
		return "country/form";
	}

	@RequestMapping("/save")
	public String save(@ModelAttribute("item") RefCountry country) {
		countryService.save(country);

		return "redirect:/country";
	}
}
