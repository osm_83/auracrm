package kz.aura.crm.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import kz.aura.crm.entity.RefCountry;
import kz.aura.crm.entity.RefRegion;
import kz.aura.crm.service.CountryService;
import kz.aura.crm.service.ReferenceService;
import kz.aura.crm.service.RegionService;

@Controller
@RequestMapping("/region")
public class RegionController {

	@Autowired
	RegionService regionService;
	
	@Autowired
	CountryService countryService;

	@RequestMapping(method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute("items", regionService.findAll());
		return "region/grid";
	}

	@RequestMapping(path = "/add", method = RequestMethod.GET)
	public String add(Model model) {
		model.addAttribute("item", new RefRegion());
		return "region/form";
	}

	@RequestMapping(path = "/update/{id}", method = RequestMethod.GET)
	public String update(@PathVariable("id") Long id, Model model) {
		RefRegion region = regionService.findOne(id);
		model.addAttribute("item", region);
		return "region/form";
	}

	@RequestMapping(value="/save",method=RequestMethod.POST)
	public String save(@ModelAttribute("item") RefRegion region) {
		regionService.save(region);

		return "redirect:/region";
	}
	
	
	@ModelAttribute("countries")
	public List<RefCountry> getCountries(){
		return countryService.findAll();
	}
}
