package kz.aura.crm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/employee")
public class EmployeeController {

	@RequestMapping(method=RequestMethod.GET)
	public String index(){
		
		return "employee/index";
	}
	
	@RequestMapping(path="/view/{id}",method=RequestMethod.GET)
	public String view(@PathVariable("id") Long id){
		
		return "employee/view";
	}
	
	@RequestMapping(value="/create",method=RequestMethod.GET)
	public String create(){
		
		return "employee/create";
	}
	
	@RequestMapping(value="/update",method=RequestMethod.GET)
	public String update(){
		
		return "employee/update";
	}
}
