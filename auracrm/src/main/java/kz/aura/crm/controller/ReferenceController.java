package kz.aura.crm.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import kz.aura.crm.entity.RefRegion;
import kz.aura.crm.service.RegionService;

@Controller
@RequestMapping("/reference")
public class ReferenceController {
	
	@Autowired
	RegionService regionService;

	public String index(){
		
		return "reference/index";
	}
	
	@RequestMapping("/get-regions-by-country-id/{countryId}")
	@ResponseBody
	public List<RefRegion> getRegionsByCountryId(@PathVariable("countryId") Long countryId){
		return regionService.findByCountryId(countryId);
	}
}
