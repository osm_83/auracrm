package kz.aura.crm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/demobank")
public class DemobankController {

	@RequestMapping(value="/demolist",method=RequestMethod.GET)
	public String demoList(){
		
		return "demobank/demolist";
	}
	
	@RequestMapping(value="/reflist",method=RequestMethod.GET)
	public String refList(){
		
		return "demobank/reflist";
	}
}
