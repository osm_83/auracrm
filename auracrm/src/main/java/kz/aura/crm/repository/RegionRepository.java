package kz.aura.crm.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import kz.aura.crm.entity.RefRegion;

public interface RegionRepository extends CrudRepository<RefRegion, Long> {
	List<RefRegion> findByCountryId(Long countryId);
}
