package kz.aura.crm.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import kz.aura.crm.entity.DocRec;

public interface RecommendationRepository extends PagingAndSortingRepository<DocRec, Long> {

}
