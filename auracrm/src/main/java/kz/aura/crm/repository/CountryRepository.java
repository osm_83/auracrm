package kz.aura.crm.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import kz.aura.crm.entity.RefCountry;

public interface CountryRepository extends PagingAndSortingRepository<RefCountry, Long> {

}
