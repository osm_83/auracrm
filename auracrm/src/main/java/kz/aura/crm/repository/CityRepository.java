package kz.aura.crm.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import kz.aura.crm.entity.RefCity;

public interface CityRepository extends PagingAndSortingRepository<RefCity, Long>,JpaSpecificationExecutor {

}
