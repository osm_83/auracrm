package kz.aura.crm.repository;

import org.springframework.data.repository.CrudRepository;

import kz.aura.crm.entity.RefDistrict;

public interface DistrictRepository extends CrudRepository<RefDistrict, Long> {

}
