package kz.aura.crm.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import kz.aura.crm.entity.DocDemo;

public interface DemoRepository extends PagingAndSortingRepository<DocDemo, Long> {

}
