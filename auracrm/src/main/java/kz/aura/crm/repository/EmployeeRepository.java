package kz.aura.crm.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import kz.aura.crm.entity.Employee;

public interface EmployeeRepository extends PagingAndSortingRepository<Employee, Long> {
	
	//List<Employee> findByBranchId(Long branchId);
}
