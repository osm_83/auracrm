package kz.aura.crm.entity;

import java.io.Serializable;
import javax.persistence.*;

import kz.aura.crm.entity.iface.IDoc;

import java.util.Date;


/**
 * The persistent class for the doc_demo database table.
 * 
 */
@Entity
@Table(name="doc_demo")
@NamedQuery(name="DocDemo.findAll", query="SELECT d FROM DocDemo d")
public class DocDemo implements Serializable,IDoc {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(updatable=false)
	private Long id;

	private String address;

	@Column(name="branch_id")
	private Long branchId;

	@Column(name="call_id")
	private Long callId;

	@Column(name="contract_number")
	private Long contractNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_at")
	private Date createdAt;

	@Column(name="created_by")
	private Long createdBy;

	@Column(name="customer_id")
	private Long customerId;

	@Column(name="customer_name")
	private String customerName;

	@Column(name="dealer_id")
	private Long dealerId;

	@Temporal(TemporalType.DATE)
	@Column(name="doc_date")
	private Date docDate;

	@Column(name="doc_number")
	private Long docNumber;

	@Column(name="doc_rec_id")
	private Long docRecId;

	private String location;

	private String note;

	@Column(name="parent_customer_name")
	private String parentCustomerName;

	@Column(name="rec_count")
	private int recCount;

	private String status;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_at")
	private Date updatedAt;

	@Column(name="updated_by")
	private Long updatedBy;

	@Version
	private int version;

	public DocDemo() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Long getBranchId() {
		return this.branchId;
	}

	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	public Long getCallId() {
		return this.callId;
	}

	public void setCallId(Long callId) {
		this.callId = callId;
	}

	public Long getContractNumber() {
		return this.contractNumber;
	}

	public void setContractNumber(Long contractNumber) {
		this.contractNumber = contractNumber;
	}

	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Long getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Long getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Long getDealerId() {
		return this.dealerId;
	}

	public void setDealerId(Long dealerId) {
		this.dealerId = dealerId;
	}

	public Date getDocDate() {
		return this.docDate;
	}

	public void setDocDate(Date docDate) {
		this.docDate = docDate;
	}

	public Long getDocNumber() {
		return this.docNumber;
	}

	public void setDocNumber(Long docNumber) {
		this.docNumber = docNumber;
	}

	public Long getDocRecId() {
		return this.docRecId;
	}

	public void setDocRecId(Long docRecId) {
		this.docRecId = docRecId;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getParentCustomerName() {
		return this.parentCustomerName;
	}

	public void setParentCustomerName(String parentCustomerName) {
		this.parentCustomerName = parentCustomerName;
	}

	public int getRecCount() {
		return this.recCount;
	}

	public void setRecCount(int recCount) {
		this.recCount = recCount;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Long getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}