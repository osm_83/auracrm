package kz.aura.crm.entity;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import kz.aura.crm.entity.iface.IReference;

import java.util.Calendar;
import java.util.Date;

/**
 * The persistent class for the ref_city database table.
 * 
 */
@Entity
@Table(name = "ref_city")
@NamedQuery(name = "RefCity.findAll", query = "SELECT r FROM RefCity r")
public class RefCity implements Serializable, IReference {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(updatable = false)
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at", updatable = false)
	private Date createdAt;

	@Column(name = "created_by", updatable = false)
	private Long createdBy;

	private String name;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_at")
	private Date updatedAt;

	@Column(name = "updated_by")
	private Long updatedBy;

	@Column(name = "region_id")
	private Long regionId;

	@Column(name = "country_id")
	private Long countryId;

	// bi-directional many-to-one association to RefRegion
	// @ManyToOne(fetch=FetchType.LAZY)
	// @JoinColumn(name="region_id")
	// private RefRegion region;

	public Long getCountryId() {
		return countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	public Long getRegionId() {
		return regionId;
	}

	public void setRegionId(Long regionId) {
		this.regionId = regionId;
	}

	// bi-directional many-to-one association to RefCountry
	// @ManyToOne(fetch = FetchType.LAZY)
	// @JoinColumn(name = "country_id")
	// private RefCountry country;

	// bi-directional many-to-one association to RefDistrict
	// @OneToMany(mappedBy = "city")
	// private List<RefDistrict> districts;

	public RefCity() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Long getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Long getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	@OneToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "country_id", insertable = false, updatable = false, referencedColumnName = "id")
	@NotFound(action=NotFoundAction.IGNORE)
	private RefCountry country;

	public RefCountry getCountry() {
		return country;
	}

	public void setCountry(RefCountry country) {
		this.country = country;
	}

	@OneToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "region_id", insertable = false, updatable = false, referencedColumnName = "id")
	@NotFound(action = NotFoundAction.IGNORE)
	private RefRegion region;

	public RefRegion getRegion() {
		return region;
	}

	public void setRegion(RefRegion region) {
		this.region = region;
	}

	@PrePersist
	public void onPrePersist() {
		setCreatedAt(Calendar.getInstance().getTime());
		setCreatedBy(1L);
		setUpdatedAt(Calendar.getInstance().getTime());
		setUpdatedBy(1L);
	}

	@PreUpdate
	public void onPreUpdate() {
		setUpdatedAt(Calendar.getInstance().getTime());
		setUpdatedBy(1L);
	}

}