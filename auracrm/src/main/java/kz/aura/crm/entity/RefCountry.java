package kz.aura.crm.entity;

import java.io.Serializable;
import javax.persistence.*;

import kz.aura.crm.entity.iface.IReference;

import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the ref_country database table.
 * 
 */
@Entity
@Table(name="ref_country")
@NamedQuery(name="RefCountry.findAll", query="SELECT r FROM RefCountry r")
public class RefCountry implements Serializable, IReference {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(updatable=false)
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_at", updatable=false)
	private Date createdAt;

	@Column(name="created_by", updatable=false)
	private Long createdBy;

	private String name;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_at")
	private Date updatedAt;

	@Column(name="updated_by")
	private Long updatedBy;

	//bi-directional many-to-one association to RefRegion
//	@OneToMany(mappedBy="country")
//	private List<RefRegion> regions;

	//bi-directional many-to-one association to RefCity
//	@OneToMany(mappedBy="country")
//	private List<RefCity> cities;

	public RefCountry() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Long getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Long getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

//	public List<RefRegion> getRegions() {
//		return this.regions;
//	}
//
//	public void setRegions(List<RefRegion> regions) {
//		this.regions = regions;
//	}
//
//	public RefRegion addRegion(RefRegion region) {
//		getRegions().add(region);
//		region.setCountry(this);
//
//		return region;
//	}
//
//	public RefRegion removeRegion(RefRegion region) {
//		getRegions().remove(region);
//		region.setCountry(null);
//
//		return region;
//	}


	@PrePersist
	public void onPrePersist(){
		setCreatedAt(Calendar.getInstance().getTime());
		setCreatedBy(1L);
		setUpdatedAt(Calendar.getInstance().getTime());
		setUpdatedBy(1L);
	}
	
	@PreUpdate
	public void onPreUpdate(){
		setUpdatedAt(Calendar.getInstance().getTime());
		setUpdatedBy(1L);
	}
}