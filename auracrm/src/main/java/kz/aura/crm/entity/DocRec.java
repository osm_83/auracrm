package kz.aura.crm.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the doc_rec database table.
 * 
 */
@Entity
@Table(name="doc_rec")
@NamedQuery(name="DocRec.findAll", query="SELECT d FROM DocRec d")
public class DocRec implements Serializable, kz.aura.crm.entity.iface.IDoc {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(updatable=false)
	private Long id;

	private String address;

	@Column(name="branch_id")
	private Long branchId;

	@Temporal(TemporalType.DATE)
	@Column(name="call_date")
	private Date callDate;

	@Temporal(TemporalType.TIME)
	@Column(name="call_time_from")
	private Date callTimeFrom;

	@Temporal(TemporalType.TIME)
	@Column(name="call_time_to")
	private Date callTimeTo;

	@Column(name="city_id")
	private Long cityId;

	@Column(name="contract_number")
	private Long contractNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_at", updatable=false)
	private Date createdAt;

	@Column(name="created_by", updatable=false)
	private Long createdBy;

	@Column(name="customer_id")
	private Long customerId;

	@Column(name="customer_name")
	private String customerName;

	@Column(name="demo_id")
	private Long demoId;

	@Column(name="district_id")
	private Long districtId;

	@Temporal(TemporalType.DATE)
	@Column(name="doc_date")
	private Date docDate;

	@Column(name="has_animal")
	private byte hasAnimal;

	@Column(name="has_child")
	private byte hasChild;

	@Column(name="home_phone")
	private String homePhone;

	private String location;

	@Column(name="mob_phone")
	private String mobPhone;

	private String note;

	@Column(name="owner_branch_id")
	private Long ownerBranchId;

	@Column(name="owner_id")
	private Long ownerId;

	@Column(name="parent_id")
	private Long parentId;

	@Column(name="responsible_id")
	private Long responsibleId;

	private String status;

	@Column(name="tree_id")
	private Long treeId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_at")
	private Date updatedAt;

	@Column(name="updated_by")
	private Long updatedBy;

	@Version
	private int version;

	@Column(name="work_phone")
	private String workPhone;

	public DocRec() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Long getBranchId() {
		return this.branchId;
	}

	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	public Date getCallDate() {
		return this.callDate;
	}

	public void setCallDate(Date callDate) {
		this.callDate = callDate;
	}

	public Date getCallTimeFrom() {
		return this.callTimeFrom;
	}

	public void setCallTimeFrom(Date callTimeFrom) {
		this.callTimeFrom = callTimeFrom;
	}

	public Date getCallTimeTo() {
		return this.callTimeTo;
	}

	public void setCallTimeTo(Date callTimeTo) {
		this.callTimeTo = callTimeTo;
	}

	public Long getCityId() {
		return this.cityId;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

	public Long getContractNumber() {
		return this.contractNumber;
	}

	public void setContractNumber(Long contractNumber) {
		this.contractNumber = contractNumber;
	}

	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Long getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Long getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Long getDemoId() {
		return this.demoId;
	}

	public void setDemoId(Long demoId) {
		this.demoId = demoId;
	}

	public Long getDistrictId() {
		return this.districtId;
	}

	public void setDistrictId(Long districtId) {
		this.districtId = districtId;
	}

	public Date getDocDate() {
		return this.docDate;
	}

	public void setDocDate(Date docDate) {
		this.docDate = docDate;
	}

	public byte getHasAnimal() {
		return this.hasAnimal;
	}

	public void setHasAnimal(byte hasAnimal) {
		this.hasAnimal = hasAnimal;
	}

	public byte getHasChild() {
		return this.hasChild;
	}

	public void setHasChild(byte hasChild) {
		this.hasChild = hasChild;
	}

	public String getHomePhone() {
		return this.homePhone;
	}

	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getMobPhone() {
		return this.mobPhone;
	}

	public void setMobPhone(String mobPhone) {
		this.mobPhone = mobPhone;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Long getOwnerBranchId() {
		return this.ownerBranchId;
	}

	public void setOwnerBranchId(Long ownerBranchId) {
		this.ownerBranchId = ownerBranchId;
	}

	public Long getOwnerId() {
		return this.ownerId;
	}

	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}

	public Long getParentId() {
		return this.parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Long getResponsibleId() {
		return this.responsibleId;
	}

	public void setResponsibleId(Long responsibleId) {
		this.responsibleId = responsibleId;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getTreeId() {
		return this.treeId;
	}

	public void setTreeId(Long treeId) {
		this.treeId = treeId;
	}

	public Date getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Long getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getWorkPhone() {
		return this.workPhone;
	}

	public void setWorkPhone(String workPhone) {
		this.workPhone = workPhone;
	}

	@Override
	public String toString() {
		return "DocRec [id=" + id + ", address=" + address + ", branchId=" + branchId + ", callDate=" + callDate
				+ ", callTimeFrom=" + callTimeFrom + ", callTimeTo=" + callTimeTo + ", cityId=" + cityId
				+ ", contractNumber=" + contractNumber + ", createdAt=" + createdAt + ", createdBy=" + createdBy
				+ ", customerId=" + customerId + ", customerName=" + customerName + ", demoId=" + demoId
				+ ", districtId=" + districtId + ", docDate=" + docDate + ", hasAnimal=" + hasAnimal + ", hasChild="
				+ hasChild + ", homePhone=" + homePhone + ", location=" + location + ", mobPhone=" + mobPhone
				+ ", note=" + note + ", ownerBranchId=" + ownerBranchId + ", ownerId=" + ownerId + ", parentId="
				+ parentId + ", responsibleId=" + responsibleId + ", status=" + status + ", treeId=" + treeId
				+ ", updatedAt=" + updatedAt + ", updatedBy=" + updatedBy + ", version=" + version + ", workPhone="
				+ workPhone + "]";
	}

	
}