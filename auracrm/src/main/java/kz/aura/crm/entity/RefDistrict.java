package kz.aura.crm.entity;

import java.io.Serializable;
import javax.persistence.*;

import kz.aura.crm.entity.iface.IReference;

import java.util.Date;


/**
 * The persistent class for the ref_district database table.
 * 
 */
@Entity
@Table(name="ref_district")
@NamedQuery(name="RefDistrict.findAll", query="SELECT r FROM RefDistrict r")
public class RefDistrict implements Serializable, IReference {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(updatable=false)
	private Long id;

	@Column(name="country_id")
	private Long countryId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_at", updatable=false)
	private Date createdAt;

	@Column(name="created_by", updatable=false)
	private Long createdBy;

	private String name;

	@Column(name="region_id")
	private Long regionId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_at")
	private Date updatedAt;

	@Column(name="updated_by")
	private Long updatedBy;

	//bi-directional many-to-one association to RefCity
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="city_id")
	private RefCity city;

	public RefDistrict() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCountryId() {
		return this.countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Long getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getRegionId() {
		return this.regionId;
	}

	public void setRegionId(Long regionId) {
		this.regionId = regionId;
	}

	public Date getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Long getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public RefCity getCity() {
		return this.city;
	}

	public void setCity(RefCity city) {
		this.city = city;
	}

}