var crmGrid = {
	gridTable : {},
	init : function() {
		this.gridTable = $('table.crm-grid');
		var sortBy = this.gridTable.find('thead').data('sort_by');
		var sortDir = this.gridTable.find('thead').data('sort_dir');
		var pageUrl = this.gridTable.find('thead').data('page_url');
		this.gridTable.find('tr:has(th)').find('th.sorting').each(function() {
			var fieldName = $(this).data('name');
			console.log(sortBy);
			if (sortBy == fieldName) {
				$(this).removeClass("sorting");
				if (sortDir == "asc") {
					$(this).addClass("sorting_asc")
				} else {
					$(this).addClass("sorting_desc")
				}
				var sd = sortDir == 'asc' ? 'desc' : 'asc';
				$(this).unbind('click').bind('click', function() {
					window.location.search = '?sd=' + sd + '&sf=' + fieldName;
				});
			} else {
				$(this).unbind('click').bind('click', function() {
					window.location.search = '?sd=asc&sf=' + fieldName;
				});
			}
		});
	}
}

$(document).ready(function() {
	crmGrid.init();
});