function loadRegionsByCountryId(countryId,handleFunction){
	var out = {};
	$.get(ctx + "/reference/get-regions-by-country-id/" + countryId,function(response){
		if(handleFunction){
			handleFunction(response);
		}
	},'json');
}