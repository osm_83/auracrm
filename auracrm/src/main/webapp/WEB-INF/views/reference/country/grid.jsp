<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<table class="table table-striped table-hover dtr-inline dataTable crm-grid">
	<thead data-sort_by="${grid.sortBy}" data-sort_dir="${grid.sortDir}" data-page_url="/country">
		<tr role="row">
			<th class="sorting" data-name="id">ID</th>
			<th class="sorting" data-name="name">Название</th>
			<th class="sorting" data-name="createdAt">Дата создания</th>
			<th class="sorting" data-name="updatedAt">Дата редактирования</th>
			<th class="sorting" data-name="createdBy">Автор</th>
			<th class="sorting" data-name="updatedBy">Редактор</th>
			<th class="sorting_disabled">Действия</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${grid.data }" var="item">
			<tr>
				<td>${item.id }</td>
				<td>${item.name }</td>
				<td><fmt:formatDate pattern="dd.MM.yyyy HH:mm"
						value="${item.createdAt }" /></td>
				<td><fmt:formatDate pattern="dd.MM.yyyy HH:mm"
						value="${item.updatedAt }" /></td>
				<td>Дилер</td>
				<td>Алматы Робоклин</td>
				<td>
					<div class="btn-group">
						<a href="<spring:url value="/country/update/${item.id }" />"
							class="btn btn-outline btn-circle btn-sm purple"> <i
							class="fa fa-edit"></i>
						</a>
						&nbsp; <a href="javascript:void(0);"
							class="btn btn-outline btn-circle btn-sm purple"> <i
							class="fa fa-trash"></i>
						</a>
					</div>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>