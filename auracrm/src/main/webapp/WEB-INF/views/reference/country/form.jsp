<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<spring:url value="/country/save" var="formSaveUrl" />
<!-- BEGIN PAGE HEADER-->
<!-- BEGIN THEME PANEL -->
<!-- BEGIN PAGE HEADER-->
<h1 class="page-title">
	Blank Page Layout <small>blank page layout</small>
</h1>
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li><i class="icon-home"></i> <a href="index.html">Home</a> <i
			class="fa fa-angle-right"></i></li>
		<li><a href="#">Blank Page</a> <i class="fa fa-angle-right"></i>
		</li>
		<li><span>Page Layouts</span></li>
	</ul>
	<div class="page-toolbar">
		<div class="btn-group pull-right">
			<button type="button"
				class="btn btn-fit-height grey-salt dropdown-toggle"
				data-toggle="dropdown" data-hover="dropdown" data-delay="1000"
				data-close-others="true">
				Actions <i class="fa fa-angle-down"></i>
			</button>
			<ul class="dropdown-menu pull-right" role="menu">
				<li><a href="#"> <i class="icon-bell"></i> Action
				</a></li>
				<li><a href="#"> <i class="icon-shield"></i> Another action
				</a></li>
				<li><a href="#"> <i class="icon-user"></i> Something else
						here
				</a></li>
				<li class="divider"></li>
				<li><a href="#"> <i class="icon-bag"></i> Separated link
				</a></li>
			</ul>
		</div>
	</div>
</div>
<!-- END PAGE HEADER-->

<div class="row">
	<div class="col-md-8 ">
		<!-- BEGIN SAMPLE FORM PORTLET-->
		<div class="portlet light ">
			<div class="portlet-title">
				<div class="caption font-red-sunglo">
					<i class="icon-settings font-red-sunglo"></i> <span
						class="caption-subject bold uppercase"> Default Form</span>
				</div>
				<div class="actions">
					<div class="btn-group">
						<a class="btn btn-sm green dropdown-toggle" href="javascript:;"
							data-toggle="dropdown"> Actions <i class="fa fa-angle-down"></i>
						</a>
						<ul class="dropdown-menu pull-right">
							<li><a href="javascript:;"> <i class="fa fa-pencil"></i>
									Edit
							</a></li>
							<li><a href="javascript:;"> <i class="fa fa-trash-o"></i>
									Delete
							</a></li>
							<li><a href="javascript:;"> <i class="fa fa-ban"></i>
									Ban
							</a></li>
							<li class="divider"></li>
							<li><a href="javascript:;"> Make admin </a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="portlet-body form">
				<form:form action="${formSaveUrl }" method="POST"
					modelAttribute="item" commandName="item">
					<form:hidden path="id" />

					<div class="form-body">
						<div class="form-group">
							<label>Название</label>
							<form:input path="name" cssClass="form-control input-lg" />
						</div>
					</div>

					<div class="form-actions">
						<button type="submit" class="btn blue">Сохранить</button>
						<button type="button" class="btn default">Cancel</button>
					</div>
				</form:form>
			</div>
		</div>
		<!-- END SAMPLE FORM PORTLET-->
		<!-- BEGIN SAMPLE FORM PORTLET-->

		<!-- END SAMPLE FORM PORTLET-->
		<!-- BEGIN SAMPLE FORM PORTLET-->

		<!-- END SAMPLE FORM PORTLET-->
	</div>
</div>