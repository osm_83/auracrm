<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<c:set var="counterFrom" value="${grid.getCounterFrom()}" />
<c:set var="counterTo" value="${grid.getCounterTo()}" />

<div class="col-md-5 col-sm-5">
	<div class="dataTables_info" id="sample_3_info" role="status"
		aria-live="polite">Всего ${grid.totalRecords} записей</div>
</div>

<div class="col-md-7 col-sm-7">
	<div class="dataTables_paginate paging_bootstrap_number pull-right">
		<ul class="pagination" style="visibility: visible;">

			<c:choose>
				<c:when test="${grid.enablePrevBtn()}">
					<li class="prev"><a
						href="<spring:url value="${grid.getPrevPageUrl()}" />"
						title="Prev"> <i class="fa fa-angle-left"></i>
					</a></li>
				</c:when>
				<c:otherwise>
					<li class="prev disabled"><a href="#" title="Prev"> <i
							class="fa fa-angle-left"></i>
					</a></li>
				</c:otherwise>
			</c:choose>
			<!-- <li class="prev disabled"><a href="#" title="Prev"> <i
				class="fa fa-angle-left"></i>
		</a></li> -->
			<c:forEach begin="${counterFrom}" end="${counterTo}" varStatus="loop">
				<spring:url value="/city" var="pageUrl">
					<spring:param name="p" value="${loop.current }" />
					<spring:param name="sf" value="${grid.sortBy }" />
					<spring:param name="sd" value="${grid.sortDir }" />
				</spring:url>
				<c:choose>
					<c:when test="${loop.current == grid.currentPage }">
						<li class="active"><a href="#">${loop.current}</a></li>
					</c:when>
					<c:otherwise>
						<li><a href="${pageUrl }">${loop.current}</a></li>
					</c:otherwise>
				</c:choose>
			</c:forEach>

			<c:choose>
				<c:when test="${grid.enableNextBtn()}">
					<li class="next"><a
						href="<spring:url value="${grid.getNextPageUrl()}" />"
						title="Next"> <i class="fa fa-angle-right"></i>
					</a></li>
				</c:when>
				<c:otherwise>
					<li class="next disabled"><a href="#" title="Next"> <i
							class="fa fa-angle-right"></i>
					</a></li>
				</c:otherwise>
			</c:choose>

			<!--
		<li class="next"><a href="?page=2" title="Next"><i
				class="fa fa-angle-right"></i></a></li> 
		<li class="next"><a href="?page=191" title="Next"><i
				class="fa fa-angle-double-right"></i></a></li> -->
		</ul>
	</div>
</div>