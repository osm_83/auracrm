<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<spring:url value="/city/save" var="formSaveUrl" />
<!-- BEGIN PAGE HEADER-->
<!-- BEGIN THEME PANEL -->
<!-- BEGIN PAGE HEADER-->
<h1 class="page-title">
	${pageTitle }
</h1>
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li><i class="icon-home"></i> <a href="<spring:url value="/" />">Главная</a> <i
			class="fa fa-angle-right"></i></li>
		<li><a href="<spring:url value="/city"/>">Города</a> 
		<i class="fa fa-angle-right"></i>
		</li>
		<li><span>${pageTitle }</span></li>
	</ul>
</div>
<!-- END PAGE HEADER-->

<div class="row">
	<div class="col-md-8 ">
		<!-- BEGIN SAMPLE FORM PORTLET-->
		<div class="portlet light ">
			<div class="portlet-body form">
				<form:form action="${formSaveUrl }" method="POST"
					modelAttribute="item" commandName="item">
					<form:hidden path="id" />

					<div class="form-body">
						<div class="form-group">
							<label>Название</label>
							<form:input path="name" cssClass="form-control input-lg" />
						</div>
					</div>

					<div class="form-body">
						<div class="form-group">
							<label>Страна</label>
							<form:select onchange="onChangeCountry($(this))" path="countryId" cssClass="form-control input-lg" id="countryId">
								<form:option value="0" label="Не выбрано"></form:option>
								<form:options items="${countries }" itemValue="id" itemLabel="name" />
							</form:select>
						</div>
					</div>
					
					<div class="form-body">
						<div class="form-group">
							<label>Область</label>
							<form:select path="regionId" cssClass="form-control input-lg" id="regionId">
								<form:option value="0" label="Не выбрано"></form:option>
								<form:options items="${regions }" itemValue="id" itemLabel="name" />
							</form:select>
						</div>
					</div>

					<div class="form-actions">
						<button type="submit" class="btn blue">Сохранить</button>
						<button type="button" class="btn default">Cancel</button>
					</div>
				</form:form>
			</div>
		</div>
		<!-- END SAMPLE FORM PORTLET-->
		<!-- BEGIN SAMPLE FORM PORTLET-->

		<!-- END SAMPLE FORM PORTLET-->
		<!-- BEGIN SAMPLE FORM PORTLET-->

		<!-- END SAMPLE FORM PORTLET-->
	</div>
</div>

<script>
	function onChangeCountry(object){
		var regionObject = $('#regionId');
		regionObject.html('<option value="0">Не выбрано</option>')
		loadRegionsByCountryId(object.val(),function(regions){
			for(var k in regions){
				regionObject.append('<option value="' + regions[k]['id'] + '">' + regions[k]['name'] + '</option>');
			}
		});
	}
</script>