<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<table class="table table-hover">
	<thead>
		<tr>
			<th>ID</th>
			<th>Название</th>
			<th>Страна</th>
			<th>Дата создания</th>
			<th>Дата редактирования</th>
			<th>Автор</th>
			<th>Редактор</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${items }" var="item">
			<tr>
				<td>${item.id }</td>
				<td>${item.name }</td>
				<td></td>
				<td><fmt:formatDate pattern="dd.MM.yyyy HH:mm"
						value="${item.createdAt }" /></td>
				<td><fmt:formatDate pattern="dd.MM.yyyy HH:mm"
						value="${item.updatedAt }" /></td>
				<td>Дилер</td>
				<td>Алматы Робоклин</td>
				<td>
					<div class="btn-group">
						<a href="<spring:url value="/region/update/${item.id }" />"
							class="btn btn-outline btn-circle btn-sm purple"> <i
							class="fa fa-edit"></i>
						</a>
					</div>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>