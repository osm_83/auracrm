<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<tiles:importAttribute name="addUrl" />
<tiles:insertAttribute name="gridPanel" />
<!-- END THEME PANEL -->
<h1 class="page-title">
	${pageTitle} <small>Справочники</small>
</h1>
<!-- END PAGE HEADER-->

<div class="row">
	<div class="col-md-12">
		<!-- BEGIN SAMPLE TABLE PORTLET-->
		<div class="portlet light ">
			<div class="portlet-title">
				<div class="caption"></div>
				<div class="actions">
					<a href="<spring:url value="${addUrl}" />"
						class="btn btn-circle btn-primary">Добавить</a>
				</div>
			</div>
			<div class="portlet-body">
				<div id="" class="dataTables_wrapper">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="dataTables_length">
								<!-- <label> <select name="sample_1_length"
									aria-controls="sample_1"
									class="form-control input-sm input-xsmall input-inline">
										<option value="5">5</option>
										<option value="10">10</option>
										<option value="15">15</option>
										<option value="20">20</option>
										<option value="-1">All</option>
								</select> entries
								</label> -->
							</div>
						</div>
						<div class="col-md-6 col-sm-12">
							<div class="dataTables_filter pull-right">
								<label>Поиск:<input type="search"
									class="form-control input-inline"></label>
							</div>
						</div>
					</div>
					<div class="table-scrollable">
						<tiles:insertAttribute name="gridBody" />
					</div>
					<div class="row">
						<tiles:insertAttribute name="gridFooter" />
					</div>
				</div>
			</div>
		</div>
		<!-- END SAMPLE TABLE PORTLET-->
	</div>
</div>