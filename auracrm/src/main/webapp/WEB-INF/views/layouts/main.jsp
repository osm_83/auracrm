<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<tiles:importAttribute name="pageJsPlugins" />
<tiles:importAttribute name="pageJsScripts" />
<tiles:importAttribute name="pageCssFiles" />
<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title><tiles:insertAttribute name="title"
		defaultValue="Добро пожаловать"></tiles:insertAttribute> | AuraCRM</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin Theme #2 for blank page layout"
	name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"
	rel="stylesheet" type="text/css" />
<link
	href="<spring:url value="/resources/global/plugins/font-awesome/css/font-awesome.min.css" />"
	rel="stylesheet" type="text/css" />
<link
	href="<spring:url value="/resources/global/plugins/simple-line-icons/simple-line-icons.min.css" />"
	rel="stylesheet" type="text/css" />
<link
	href="<spring:url value="/resources/global/plugins/bootstrap/css/bootstrap.min.css" />"
	rel="stylesheet" type="text/css" />
<link
	href="<spring:url value="/resources/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" />"
	rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link
	href="<spring:url value="/resources/global/css/components.min.css" />"
	rel="stylesheet" id="style_components" type="text/css" />
<link
	href="<spring:url value="/resources/global/css/plugins.min.css" />"
	rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->

<!-- BEGIN PAGE LEVEL STYLES -->
<c:forEach var="css" items="${pageCssFiles}">
	<link href="<c:url value="${css }"/>" rel="stylesheet"
		type="text/css" />
</c:forEach>
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN THEME LAYOUT STYLES -->
<link
	href="<spring:url value="/resources/layouts/layout2/css/layout.min.css" />"
	rel="stylesheet" type="text/css" />
<link
	href="<spring:url value="/resources/layouts/layout2/css/themes/blue.min.css" />"
	rel="stylesheet" type="text/css" id="style_color" />
<link
	href="<spring:url value="/resources/layouts/layout2/css/custom.min.css" />"
	rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon" href="favicon.ico" />
<script>var ctx = "${pageContext.request.contextPath}";</script>
</head>
<!-- END HEAD -->

<body
	class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">
	<!-- BEGIN HEADER -->
	<tiles:insertAttribute name="header" />
	<!-- END HEADER -->
	<!-- BEGIN HEADER & CONTENT DIVIDER -->
	<div class="clearfix"></div>
	<!-- END HEADER & CONTENT DIVIDER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<tiles:insertAttribute name="leftSidebar" />
		<!-- END SIDEBAR -->
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<!-- BEGIN CONTENT BODY -->
			<div class="page-content">
				<tiles:insertAttribute name="body" />
			</div>
			<!-- END CONTENT BODY -->
		</div>
		<!-- END CONTENT -->
		<!-- BEGIN QUICK SIDEBAR -->

		<!-- END QUICK SIDEBAR -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<tiles:insertAttribute name="footer" />
	<!-- END FOOTER -->
	<!-- BEGIN QUICK NAV -->
	<!-- TODO QUICKNAV -->
	<!-- END QUICK NAV -->
	<!--[if lt IE 9]>
<script src="<spring:url value="/resources/global/plugins/respond.min.js" />"></script>
<script src="<spring:url value="/resources/global/plugins/excanvas.min.js" />"></script> 
<script src="<spring:url value="/resources/global/plugins/ie8.fix.min.js" />"></script> 
<![endif]-->
<div id="app-ajax-modal" class="modal fade" tabindex="-1"> </div>
	<!-- BEGIN CORE PLUGINS -->
	<script
		src="<spring:url value="/resources/global/plugins/jquery.min.js" />"
		type="text/javascript"></script>
	<script
		src="<spring:url value="/resources/global/plugins/bootstrap/js/bootstrap.min.js" />"
		type="text/javascript"></script>
	<script
		src="<spring:url value="/resources/global/plugins/js.cookie.min.js" />"
		type="text/javascript"></script>
	<script
		src="<spring:url value="/resources/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" />"
		type="text/javascript"></script>
	<script
		src="<spring:url value="/resources/global/plugins/jquery.blockui.min.js" />"
		type="text/javascript"></script>
	<script
		src="<spring:url value="/resources/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" />"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->

	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<c:forEach var="pjs" items="${pageJsPlugins}">
		<script src="<c:url value="${pjs}"/>" type="text/javascript"></script>
	</c:forEach>
	<!-- END PAGE LEVEL PLUGINS -->

	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script
		src="<spring:url value="/resources/scripts/app.js" />"
		type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->

	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<c:forEach var="pjs" items="${pageJsScripts}">
		<script src="<c:url value="${pjs}"/>" type="text/javascript"></script>
	</c:forEach>
	<!-- END PAGE LEVEL SCRIPTS -->


	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script
		src="<spring:url value="/resources/layouts/layout2/scripts/layout.min.js" />"
		type="text/javascript"></script>
	<script
		src="<spring:url value="/resources/layouts/layout2/scripts/demo.min.js" />"
		type="text/javascript"></script>
	<script
		src="<spring:url value="/resources/layouts/global/scripts/quick-sidebar.min.js" />"
		type="text/javascript"></script>
	<script
		src="<spring:url value="/resources/layouts/global/scripts/quick-nav.min.js" />"
		type="text/javascript"></script>
	<!-- END THEME LAYOUT SCRIPTS -->
</body>

</html>