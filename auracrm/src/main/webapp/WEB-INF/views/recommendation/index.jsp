<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- BEGIN PAGE HEADER-->
<!-- BEGIN THEME PANEL -->
<!-- BEGIN PAGE HEADER-->
<!-- BEGIN THEME PANEL -->
<div class="theme-panel">
	<div class="toggler tooltips" data-container="body"
		data-placement="left" data-html="true"
		data-original-title="Click to open advance theme customizer panel">
		<i class="icon-settings"></i>
	</div>
	<div class="toggler-close">
		<i class="icon-close"></i>
	</div>
	<div class="theme-options">
		<div class="theme-option theme-colors clearfix">
			<span> THEME COLOR </span>
			<ul>
				<li class="color-default current tooltips" data-style="default"
					data-container="body" data-original-title="Default"></li>
				<li class="color-grey tooltips" data-style="grey"
					data-container="body" data-original-title="Grey"></li>
				<li class="color-blue tooltips" data-style="blue"
					data-container="body" data-original-title="Blue"></li>
				<li class="color-dark tooltips" data-style="dark"
					data-container="body" data-original-title="Dark"></li>
				<li class="color-light tooltips" data-style="light"
					data-container="body" data-original-title="Light"></li>
			</ul>
		</div>
		<div class="theme-option">
			<span> Theme Style </span> <select
				class="layout-style-option form-control input-small">
				<option value="square" selected="selected">Square corners</option>
				<option value="rounded">Rounded corners</option>
			</select>
		</div>
		<div class="theme-option">
			<span> Layout </span> <select
				class="layout-option form-control input-small">
				<option value="fluid" selected="selected">Fluid</option>
				<option value="boxed">Boxed</option>
			</select>
		</div>
		<div class="theme-option">
			<span> Header </span> <select
				class="page-header-option form-control input-small">
				<option value="fixed" selected="selected">Fixed</option>
				<option value="default">Default</option>
			</select>
		</div>
		<div class="theme-option">
			<span> Top Dropdown</span> <select
				class="page-header-top-dropdown-style-option form-control input-small">
				<option value="light" selected="selected">Light</option>
				<option value="dark">Dark</option>
			</select>
		</div>
		<div class="theme-option">
			<span> Sidebar Mode</span> <select
				class="sidebar-option form-control input-small">
				<option value="fixed">Fixed</option>
				<option value="default" selected="selected">Default</option>
			</select>
		</div>
		<div class="theme-option">
			<span> Sidebar Style</span> <select
				class="sidebar-style-option form-control input-small">
				<option value="default" selected="selected">Default</option>
				<option value="compact">Compact</option>
			</select>
		</div>
		<div class="theme-option">
			<span> Sidebar Menu </span> <select
				class="sidebar-menu-option form-control input-small">
				<option value="accordion" selected="selected">Accordion</option>
				<option value="hover">Hover</option>
			</select>
		</div>
		<div class="theme-option">
			<span> Sidebar Position </span> <select
				class="sidebar-pos-option form-control input-small">
				<option value="left" selected="selected">Left</option>
				<option value="right">Right</option>
			</select>
		</div>
		<div class="theme-option">
			<span> Footer </span> <select
				class="page-footer-option form-control input-small">
				<option value="fixed">Fixed</option>
				<option value="default" selected="selected">Default</option>
			</select>
		</div>
	</div>
</div>
<!-- END THEME PANEL -->
<h1 class="page-title">
	Рекомендации <small>Звонки</small>
</h1>
<!-- END PAGE HEADER-->

<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light ">
			<div class="portlet-title">
				<div class="caption font-dark">
					<i class="icon-settings font-dark"></i> <span
						class="caption-subject bold uppercase">Рекомендации</span>
				</div>
				<div class="tools"></div>
			</div>
			<div class="portlet-body">
				<table class="table table-striped table-bordered table-hover"
					id="recommendation-data-table">
					<thead>
						<tr>
							<th>Клиент</th>
							<th>Рекомендатель</th>
							<th>Желательное дата звонка</th>
							<th>Тел. Номера</th>
							<th>Адрес</th>
							<th>Ответственный</th>
							<th>Статус</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>Клиент</th>
							<th>Рекомендатель</th>
							<th>Желательное дата звонка</th>
							<th>Тел. Номера</th>
							<th>Адрес</th>
							<th>Ответственный</th>
							<th>Статус</th>
						</tr>
					</tfoot>
					<tbody>
						<c:forEach items="${items}" var="item">
							<tr>
								<td>${item.customerName}</td>
								<td>Customer Support</td>
								<td>${item.callDate}</td>
								<td>
									${item.homePhone}<br/>
									${item.workPhone}<br/>
									${item.mobPhone}
								</td>
								<td>${item.address}</td>
								<td>$112,000</td>
								<td>Status</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>