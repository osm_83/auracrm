<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<spring:url value="/recommendation/save" var="formSaveUrl" />

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"
		aria-hidden="true"></button>
	<h4 class="modal-title">Добавление рекомендации</h4>
</div>
<div class="modal-body">
	<div class="row">
		<div class="col-md-12 ">
			<!-- BEGIN SAMPLE FORM PORTLET-->
			<div class="portlet-body form">
				<form:form action="${formSaveUrl }" method="POST"
					modelAttribute="item" commandName="item" id="recommendation-form">
					<form:hidden path="id" />

					<div class="form-body">
						<div class="form-group">
							<label>Дилер</label>
							<form:select path="ownerId" cssClass="form-control">
								<form:option value="0" label="Не выбрано"></form:option>
								<form:options items="${dealers}" itemValue="id" itemLabel="value" />
							</form:select>
						</div>
						
						<div class="form-group">
							<label>ФИО Клиента</label>
							<form:input path="customerName" cssClass="form-control" />
						</div>

						<div class="form-group">
							<label>Моб. тел</label>
							<form:input path="mobPhone" cssClass="form-control" />
						</div>

						<div class="form-group">
							<label>Дом. тел</label>
							<form:input path="homePhone" cssClass="form-control" />
						</div>

						<div class="form-group">
							<label>Раб. тел</label>
							<form:input path="workPhone" cssClass="form-control" />
						</div>

						<div class="form-group">
							<label>Адрес</label>
							<form:textarea path="address" cssClass="form-control" />
						</div>
	

					</div>
				</form:form>
			</div>
		</div>
	</div>
</div>
<div class="modal-footer">
	<button type="button" class="btn default" data-dismiss="modal">Отмена</button>
	<button id="rec-form-save-btn" type="button" class="btn blue">Сохранить</button>
</div>

<script>
	$(document).ready(function() {
		$('#rec-form-save-btn').unbind('click').bind('click', function() {
			var paramObj = {};
			$.each($('#recommendation-form').serializeArray(), function(_, kv) {
			  if (paramObj.hasOwnProperty(kv.name)) {
			    paramObj[kv.name] = $.makeArray(paramObj[kv.name]);
			    paramObj[kv.name].push(kv.value);
			  }
			  else {
			    paramObj[kv.name] = kv.value;
			  }
			});
			$.ajax({
                url : ctx + '/recommendation/save',
                data : JSON.stringify(paramObj),
                method : 'POST',
                contentType : "application/json",
                success : function(data) {

                },
                error : function(data) {

                }
            });
		});
	});
</script>